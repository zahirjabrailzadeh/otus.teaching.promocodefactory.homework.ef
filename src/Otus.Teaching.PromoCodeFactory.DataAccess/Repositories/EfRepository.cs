﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly DataContext context;

        public EfRepository(DataContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task CreateAsync(T entity)
        {
            await context.AddAsync<T>(entity);
        }

        public async Task DeleteAsync(T entity)
        {
            context.Remove<T>(entity);
        }

        public async Task<IEnumerable<T>> GetAllAsync(string[] includeParams = null)
        {
            IQueryable<T> entities = context.Set<T>();
            if (includeParams != null && includeParams.Length > 0)
            {
                foreach (var param in includeParams)
                {
                    entities = entities.Include<T>(param);
                }
            }

            return await entities.ToListAsync<T>();
        }

        public async Task<T> GetByIdAsync(Guid id, string[] includeParams = null)
        {
            IQueryable<T> entities = context.Set<T>();
            if (includeParams != null && includeParams.Length > 0)
            {
                foreach (var param in includeParams)
                {
                    entities = entities.Include<T>(param);
                }
            }
            var item = await entities.FirstOrDefaultAsync(e => e.Id == id);
            return item;
        }

        public async Task UpdateAsync(T entity)
        {
            context.Update<T>(entity);
        }

        public async Task<bool> SaveAll()
        {
            return await context.SaveChangesAsync() > 0;
        }
    }
}
