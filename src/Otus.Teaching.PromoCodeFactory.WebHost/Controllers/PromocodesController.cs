﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController : ControllerBase
    {
        private readonly IRepository<PromoCode> _promoCodeRepo;
        private readonly IRepository<Preference> _preferenceRepo;
        private readonly IMapper _mapper;

        public PromocodesController(IRepository<PromoCode> promoCodeRepo, IRepository<Preference> preferenceRepo, IMapper mapper)
        {
            _promoCodeRepo = promoCodeRepo ?? throw new ArgumentNullException(nameof(promoCodeRepo));
            _preferenceRepo = preferenceRepo ?? throw new ArgumentNullException(nameof(preferenceRepo));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promoCodes = await _promoCodeRepo.GetAllAsync();

            if (!promoCodes.Any())
            {
                return NotFound();
            }

            var promoCodesToReturn = _mapper.Map<IEnumerable<PromoCodeShortResponse>>(promoCodes);
            return Ok(promoCodesToReturn);
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var allPreferences = await _preferenceRepo.GetAllAsync(new string[] { "Customers" });
            var preference = allPreferences.FirstOrDefault(p => p.Name == request.Preference);

            if (preference == null)
            {
                return NotFound();
            }

            foreach (var item in preference.Customers)
            {
                var promoCode = _mapper.Map<PromoCode>(request);
                _mapper.Map(item, promoCode);
                _mapper.Map(preference, promoCode);
                await _promoCodeRepo.CreateAsync(promoCode);
            }
            
            if (await _promoCodeRepo.SaveAll())
            {
                return Ok();
            }

            return BadRequest("Failed to create the promo code");
        }
    }
}