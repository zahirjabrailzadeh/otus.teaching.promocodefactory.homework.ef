﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController : ControllerBase
    {
        private readonly IRepository<Preference> _preferenceRepo;
        private readonly IMapper _mapper;

        public PreferencesController(IRepository<Preference> preferenceRepo, IMapper mapper)
        {
            _preferenceRepo = preferenceRepo ?? throw new ArgumentNullException(nameof(preferenceRepo));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        /// <summary>
        /// Получить список клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PreferenceResponse>>> GetPreferencesAsync()
        {
            var preferences = await _preferenceRepo.GetAllAsync();
            if (!preferences.Any())
            {
                return NotFound();
            }

            var preferencesToReturn = _mapper.Map<IEnumerable<PreferenceResponse>>(preferences);
            return Ok(preferencesToReturn);
        }
    }
}
