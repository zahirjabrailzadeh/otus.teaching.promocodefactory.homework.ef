﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<Customer, CustomerShortResponse>();
            CreateMap<CustomerShortResponse, Customer>();
            CreateMap<Customer, CustomerResponse>();
            CreateMap<CustomerResponse, Customer>();
            CreateMap<Customer, CreateOrEditCustomerRequest>();
            CreateMap<CreateOrEditCustomerRequest, Customer>();
            CreateMap<IEnumerable<Preference>, Customer>()
                .ForMember(dest => dest.Preferences, opt => opt.MapFrom(src => src))
                .ForAllOtherMembers(opt => opt.Ignore());

            CreateMap<PromoCode, PromoCodeShortResponse>();
            CreateMap<PromoCodeShortResponse, PromoCode>();

            CreateMap<Preference, PreferenceResponse>();
            CreateMap<PreferenceResponse, Preference>();

            CreateMap<GivePromoCodeRequest, PromoCode>()
                .ForMember(dest => dest.Code, opt => opt.MapFrom(src => src.PromoCode))
                .ForMember(dst => dst.Customer, opt => opt.Ignore())
                .ForMember(dst => dst.Preference, opt => opt.Ignore());
            CreateMap<Customer, PromoCode>()
                .ForMember(dest => dest.Customer, opt => opt.MapFrom(src => src))
                .ForAllOtherMembers(opt => opt.Ignore());
            CreateMap<Preference, PromoCode>()
                .ForMember(dest => dest.Preference, opt => opt.MapFrom(src => src))
                .ForAllOtherMembers(opt => opt.Ignore());
        }
    }
}
