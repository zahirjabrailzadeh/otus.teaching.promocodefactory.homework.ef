﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepo;
        private readonly IRepository<Preference> _preferenceRepo;
        private readonly IRepository<PromoCode> _promoCodeRepo;
        private readonly IMapper _mapper;

        public CustomersController(IRepository<Customer> customerRepo, IRepository<Preference> preferenceRepo, IRepository<PromoCode> promoCodeRepo, IMapper mapper)
        {
            _customerRepo = customerRepo ?? throw new ArgumentNullException(nameof(customerRepo));
            _preferenceRepo = preferenceRepo ?? throw new ArgumentNullException(nameof(preferenceRepo));
            _promoCodeRepo = promoCodeRepo ?? throw new ArgumentNullException(nameof(promoCodeRepo));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        /// <summary>
        /// Получить список клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CustomerShortResponse>>> GetCustomersAsync()
        {
            var customers = await _customerRepo.GetAllAsync();
            if (!customers.Any())
            {
                return NotFound();
            }

            var customersToReturn = _mapper.Map<IEnumerable<CustomerShortResponse>>(customers);
            return Ok(customersToReturn);
        }

        /// <summary>
        /// Получить данные клиента по идентификатору
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}", Name = "GetCustomerAsync")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepo.GetByIdAsync(id, new string[] { "PromoCodes", "Preferences" });
            if (customer == null)
            {
                return NotFound();
            }

            var customerToReturn = _mapper.Map<CustomerResponse>(customer);
            return Ok(customerToReturn);
        }
        
        /// <summary>
        /// Создать нового клиента
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var preferences = await _preferenceRepo.GetAllAsync();        
            var customer = _mapper.Map<Customer>(request);
            _mapper.Map(preferences.Where(p => request.PreferenceIds.Contains(p.Id)), customer);

            await _customerRepo.CreateAsync(customer);

            if (await _customerRepo.SaveAll())
            {
                var customerToReturn = _mapper.Map<CustomerResponse>(customer);
                return CreatedAtRoute("GetCustomerAsync", new { id = customer.Id }, customerToReturn);
            }

            return BadRequest("Failed to create the customer");
        }
        
        /// <summary>
        /// Изменить данные клиента по идентификатору
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepo.GetByIdAsync(id, new string[] { "Preferences" });
            if (customer == null)
            {
                return NotFound(); 
            }

            customer.Preferences.Clear(); 
            var preferences = await _preferenceRepo.GetAllAsync();
            _mapper.Map(request, customer);
            _mapper.Map(preferences.Where(p => request.PreferenceIds.Contains(p.Id)), customer);

            await _customerRepo.UpdateAsync(customer);

            if (await _customerRepo.SaveAll())
            {
                return NoContent();
            }

            return BadRequest($"Failed to update customer with {id}");
        }

        /// <summary>
        /// Удалить клиента вместе с выданными ему промокодами по идентификатору
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customerRepo.GetByIdAsync(id, new string[] { "PromoCodes" });
            if (customer == null)
            {
                return NotFound();
            }
            foreach (var promoCode in customer.PromoCodes)
            {
                await _promoCodeRepo.DeleteAsync(promoCode);
            }

            await _customerRepo.DeleteAsync(customer);

            if (await _customerRepo.SaveAll())
            {
                await _promoCodeRepo.SaveAll();
                return NoContent();
            }
            return BadRequest($"Failed to delete customer with {id}");
        }
    }
}