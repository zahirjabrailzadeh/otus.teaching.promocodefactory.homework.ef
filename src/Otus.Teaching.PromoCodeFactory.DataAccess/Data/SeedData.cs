﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static class SeedData
    {
        public async static void SeedDatabase(DataContext context)
        {
            //await context.Database.EnsureDeletedAsync();
            await context.Database.MigrateAsync();

            if (
                   !await context.Employees.AnyAsync()
                || !await context.Customers.AnyAsync()
                )
            {
                await context.Employees.AddRangeAsync(FakeDataFactory.Employees);
                await context.Customers.AddRangeAsync(FakeDataFactory.Customers);
                await context.SaveChangesAsync();
            }
        }
    }
}
